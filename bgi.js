﻿const fs = require('fs');
const path = require('path');
const sizeOf = require('image-size');
const img_re = new RegExp('(.+)\.(png|jpg|jpeg|gif|webp)$','i');
const tmpl_less = `@%NAME%-width:%WIDTH%px;
@%NAME%-height:%HEIGHT%px;
@%NAME%-image:url("%PATH%");
.%NAME%(){
	background-image:url("%PATH%");
	background-size:%WIDTH%px %HEIGHT%px;
}
.%NAME%--block(){
	background-image:url("%PATH%");
	background-size:%WIDTH%px %HEIGHT%px;
	width:%WIDTH%px;
	height:%HEIGHT%px;
}
`;
const tmpl_sass = `$%NAME%-width:%WIDTH%px;
$%NAME%-height:%HEIGHT%px;
$%NAME%-image:url("%PATH%");
@mixin %NAME%(){
	background-image:url("%PATH%");
	background-size:%WIDTH%px %HEIGHT%px;
}
@mixin %NAME%--block(){
	background-image:url("%PATH%");
	background-size:%WIDTH%px %HEIGHT%px;
	width:%WIDTH%px;
	height:%HEIGHT%px;
}
`;
let walk = function(dir, callback) {
    let results = [];
    fs.readdir(dir, function(err, list) {
        if (err) {
            return callback(err);
        }
        let pending = list.length;
        if (!pending) {
            return callback(null, results);
        }
        list.forEach(function(file) {
            file = path.resolve(dir, file);
            fs.stat(file, function(err, stat) {
                if (stat && stat.isDirectory()) {
                    walk(file, function(err, res) {
                        results = results.concat(res);
                        if (!--pending) {
                            callback(null, results);
                        }
                    });
                } else {
                    results.push(file);
                    if (!--pending) {
                        callback(null, results);
                    }
                }
            });
        });
    });
};
let generate = function(option) {
    let isRetina = false;
    let pathVar;
    let outFile;
    let type;
    let src = process.cwd();
    if (typeof option === 'object') {
        if (option.isRetina) {
            isRetina = true;
        }
        if (option.pathVar) {
            pathVar = option.pathVar;
        }
        if (option.outFile) {
            outFile = option.outFile;
        }
        if (option.type) {
            type = option.type;
        }
        if (option.src){
            src = option.src;
        }
    }
    // windowsの区切り文字対策
    src = src.split(path.sep).join('/');
    if(src.slice(-1) !== '/'){ // パスの最後が区切り文字でなければ区切り文字を足す
    	src += '/';
    }
    let filename;
    let tmpl;
    let interpolation;
    if (!type || type === 'less') {
        filename = outFile ? outFile : 'images.less';
        tmpl = tmpl_less;
        interpolation = pathVar ? '@{' + pathVar + '}' : '';
    } else if (type === 'sass') {
        filename = outFile ? outFile : 'images.scss';
        tmpl = tmpl_sass;
        interpolation = pathVar ? '#{$' + pathVar + '}' : '';
    }
    let resText = '';
    walk(src, function(err, results) {
        if (err) throw err;
        results.filter(function(file) {
            return fs.statSync(file).isFile() && img_re.test(file); // 絞り込み
        }).sort().forEach(function(file) {
            let imgData = sizeOf(file);
            let w = imgData.width;
            let h = imgData.height;
            if (isRetina) {
                w = Math.ceil(w / 2);
                h = Math.ceil(h / 2);
            }
            let basename = path.relative(src, file);
            let matches = basename.match(img_re);
            let name = matches[1].split(path.sep).join('__'); // サブフォルダはアンダースコア2つでつなぐ
            let res = tmpl.replace(/%NAME%/g, name)
                .replace(/%WIDTH%/g, w)
                .replace(/%HEIGHT%/g, h);
            resText += res.replace(/%PATH%/g, interpolation + basename.split(path.sep).join('/'));
        });
        let outDir = src;
        if(typeof option !== 'object' || !option.src){ // ソース指定が無ければカレントフォルダ
            outDir = 'current directory';
        }
        console.log(filename + ' is generated at ' + outDir + '.');
        fs.writeFileSync(src+filename, resText);
    });
}
module.exports = generate;
