# bgi

> Generates width, height and path variables and mixins of the images in specified directory.

Install this globally and you'll have access to the `bgi` command anywhere on your system.

```shell
npm install bgi -g
```

## Example
### LESS
```sh
bgi --pathVar imagepath --type less

```
```less
@icon_Addlink-width:38px;
@icon_Addlink-height:37px;
@icon_Addlink-image:url("@{imagepath}icon_Addlink.png");
.icon_Addlink(){
  background-image:url("@{imagepath}icon_Addlink.png");
  background-size:38px 37px;
}
.icon_Addlink--block(){
  background-image:url("@{imagepath}icon_Addlink.png");
  background-size:38px 37px;
  width:38px;
  height:37px;
}
```
### SASS
```sh
bgi --pathVar imagepath --type sass

```
```scss
$icon_Addlink-width:38px;
$icon_Addlink-height:37px;
$icon_Addlink-image:url("#{$imagepath}icon_Addlink.png");
@mixin icon_Addlink(){
  background-image:url("#{$imagepath}icon_Addlink.png");
  background-size:38px 37px;
}
@mixin icon_Addlink--block(){
  background-image:url("#{$imagepath}icon_Addlink.png");
  background-size:38px 37px;
  width:38px;
  height:37px;
}
```

## Supported formats

* GIF
* JPEG
* PNG
* WebP

## Command-Line Usage (CLI)

```sh
cd pathToImageDir
bgi --pathVar imagepath --type sass --retina

```
## Options
### src
Type: `String`
Default: `null`

Specifies the source directory to be processed.
Current directory will be used if not specified.

### pathVar
Type: `String`
Default: `null`

A string of the variable name which will be replaced with the path to the source directory.
System file path will be used if not specified.

### type
Type: `String`
Default: `less`

Determines the output format of the final CSS style. The Value can be either 'less' or 'sass' at this time.

### retina
Type: `Boolean`
Default: `false`

Determines if images are for retina or not. If set to true, width and height values will be devided by 2.

### outFile
Type: `String`
Default: `images.less` OR `images.sass`

Determines the name of output file.

##Usage
###LESS
```less
@imagepath:"../img/mypage/";
@import "@{imagepath}images.less";
div.icon--addlink{
  .icon_Addlink--block();
}
span.icon--addlink{
  .icon_Addlink();
}
```
###SASS
```scss
$imagepath:"../img/mypage/";
@import "#{$imagepath}images.scss";
div.icon--addlink{
  @include icon_Addlink--block();
}
span.icon--addlink{
  @include icon_Addlink();
}
```